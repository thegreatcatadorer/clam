(defpackage :classes
  (:use :cl :modules :symbols :macros :anaphors)
  (:export "PURE" "BIND" "QUEUE" "MAKE-QUEUE" "NEXT" "ADD" "DEFEVENT"
           "MAKE-EVENT" "EVENT-PUSH" "EVENT!" "UPDATE" "COLLECT-ITEM"
           "FOR-HASH" "ADD-SUBSET"))
(in-package :classes)

(defclass maybe () ())
(defclass nothing (maybe) ())
(defclass just (maybe)
  ((value :initarg :value)))

(defmethod from-maybe ((value nothing) default)
  (declare (ignore value))
  default)
(defmethod from-maybe ((value just) default)
  (slot-value 'value value))

(declaim (inline as-just make-nothing))
(defun as-just (value)
  (make-instance 'just :value value))
(defun make-nothing ()
  (make-instance 'nothing))

(defgeneric pure (value monad)
  (:documentation "Returns a function to lift a value into the monad."))
(defgeneric bind (function monad)
  (:documentation "Lifts a function into the monad."))

(defmethod pure (value (monad (eql 'list)))
  (cons value nil))
(defmethod bind (function (monad list))
  (mapcan function monad))

(defmethod pure (value (monad (eql 'maybe)))
  (as-just value))
(defmethod bind (function (monad nothing))
  (declare (ignore function))
  monad)
(defmethod bind (function (monad just))
  (as-just (funcall function (slot-value 'value monad))))

(defmacro for-hash ((key value table) &body body)
  `(block nil (maphash (lambda (,key ,value) . ,body) ,table)))

(defun hash-diff (set1 set2)
  (for-hash (key value set2)
    (declare (ignore value))
    (remhash key set1))
  set2)

(defun in-hash-p (key hash)
  (nth-value 1 (gethash key hash)))

(defmethod next ((iterator list))
  (pop iterator))
(defmethod add ((iterator list) &rest values)
  (setf iterator (nconc values iterator)))

(defclass queue ()
  ((front-ptr :initarg :list :accessor queue-list)
   (rear-ptr :initform nil)))

(defmethod next ((iterator queue))
  (with-slots (front-ptr) iterator
    (pop front-ptr)))
(defmethod add ((iterator queue) &rest values)
  (with-slots (front-ptr rear-ptr) iterator
    (dolist (b values)
      (let ((pair (cons b nil)))
        (if (null front-ptr)
            (progn (setf front-ptr pair)
                   (setf rear-ptr pair))
            (progn (setf (cdr rear-ptr) pair)
                   (setf rear-ptr (cdr rear-ptr))))))))

(defun make-queue (&optional list)
  (let ((queue (make-instance 'queue :list list)))
    (when list (apply #'add queue list)) queue))

(defmethod pure (value (monad (eql 'queue)))
  (make-queue (list value)))
(defmethod bind (function (monad queue))
  (bind function (queue-list monad)))

(defmacro doqueue ((item queue &optional list) &body body)
  `(let ,(if list `((,queue (make-queue ,list))) nil)
     (loop
       (if (queue-list ,queue)
           (let ((,item (next ,queue))) ,@body)
           (return)))))

(defclass event ()
  ((functions :initarg :functions :accessor event-functions)))
(defun make-event (&rest functions)
  (make-instance 'event :functions functions))
(defun event-push (function event)
  (push function (event-functions event)))
(defun event! (event &rest values)
  (dolist (e (event-functions event))
    (apply e values)))

(defmacro defevent (name &rest args)
  `(setf ,name (make-event ,@args)))

;;; An object that collects all associations from its sources.
(defclass collector ()
  ((set :initarg set :initform (make-hash-table) :accessor collector-set)
   (hook :initform (make-event) :accessor collector-hook)))
(defun update (collector pairs)
  (with-slots (set hook) collector
    (let (update-outputs)
      (map 'list
        (lambda (pair)
          (destructuring-bind (key value) pair
            (unless #1=(gethash key set)
              (setf #1# value)
              (push pair update-outputs))))
        pairs)
      (when update-outputs
        (event! hook update-outputs)))))
(defun collect-item (item collector &optional (value t))
  (update collector `((,item ,value))))
(defun add-subset (sub super)
  (with-slots (set hook) sub
    (event-push (lambda (pairs) (update super pairs))
                hook)
    (for-hash (key value set)
      (update super `((,key ,value))))))
