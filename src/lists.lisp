(defpackage :lists
  (:use :cl :modules :symbols :macros :anaphors)
  (:export "FLATTEN" "TAKE" "DROP" "INSERT-AT" "CIRCULAR-LIST-P" "SUBLIST-P"
           "CARS" "CDRS" "MAP-OR"))
(in-package :lists)

(defun flatten (tree)
  (labels ((recur (tree acc)
             (cond ((null tree) acc)
                   ((atom tree) (cons tree acc))
                   ((consp tree) (recur (cdr tree) (recur (car tree) acc))))))
    (nreverse (recur tree nil))))

(defun take (i list) ;; Also functions as split.
  (tail-recursive recur ((i i) (list list) (acc nil))
    (if (zerop i) (values (nreverse acc) list)
        (recur (- i 1) (cdr list) (cons (car list) acc)))))

(defun drop (i list)
  (tail-recursive recur ((i i) (list list))
    (if (zerop i) list
        (recur (- i 1) (cdr list)))))

(defun insert-at (i value list)
  (tail-recursive recur ((i i) (list list) (acc nil))
    (if (zerop i) (append (nreverse acc) (cons value list))
        (recur (- i 1) (cdr list) (cons (car list) acc)))))

(defun circular-list-p (list) ;; Checks lists for circularity in constant space.
  (tail-recursive recur ((cons nil) (i 0) (span 0) (list list))
    (cond ((eq cons list) t)
          ((null list) nil)
          ((zerop i) (recur list (ash 1 span) (+ span 1) (cdr list)))
          (t (recur cons (- i 1) span (cdr list))))))

(defun sublist-p (list0 list1)
  (tail-recursive recur ((list0 list0) (list1 list1))
    (cond ((null list0) t)
          ((null list1) nil)
          ((eql (car list0) (car list1))
           (recur (cdr list0) (cdr list1))))))

(defun cars (list)
  (map 'list #'car list))
(defun cdrs (list)
  (map 'list #'cdr list))

(defun map-or (function &rest sequences)
  (tail-recursive recur ((sequences sequences))
    (and (every (lambda (s) (nth-value 1 (ignore-errors (elt s 0))))
                sequences)
      (or (apply function (mapcar (lambda (s) (elt s 0)) sequences))
          (recur (mapcar (lambda (s) (subseq s 1)) sequences))))))
