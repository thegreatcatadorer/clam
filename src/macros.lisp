(defpackage :macros
  (:use :cl :modules :symbols)
  (:export "INJECT" "WITH-BINDINGS-DO" "GENSYMS" "COMPOSE1" "LAMBDA-MEMO" "DEFMEMO"
           "NEW-TREE-TRAVERSE" "TREE-TRAVERSE" "TREE-LEAVES" "FN"
           "MAP-DESTRUCTURING" "TRANSFORM" "TEMPLATE" "AFTER-INJECTION"
           "CAPTURE"))
(in-package :macros)

(defmacro inject (&body body)
  (eval `(progn . ,body)))

(defmacro transform (function &body body)
  (eval `(apply ,function ',body)))

(defmacro after-injection (function &body body)
  `(,function . ,(eval `(progn . ,body))))

(defmacro template ((form-binding &rest expansion) &body body)
  `(inject (map 'list (lambda (,form-binding) . ,expansion) ',body)))

(defmacro with-bindings-do ((pattern &rest bindings) &body body)
  `(inject
     (list 'progn
           . ,(map 'list
                   (lambda (bs) `(let ,(map 'list #'list pattern bs) . ,body))
                bindings))))

(defmacro gensyms (bindings &body body)
  `(let ,(mapcar (lambda (b) 
                   (if (consp b) b
                       `(,b (gensym ,(symbol-name b)))))
                 bindings)
     ,@body))

(defmacro compose1 (&rest fs)
  (gensyms (#1=#:arg1)
    (loop with acc = #1#
          for f in (reverse fs)
          do (setf acc (list f acc))
          finally (return `(lambda (,#1#) ,acc)))))

(defmacro lambda-memo (lambda-list &body body)
  (gensyms (table args)
    `(let ((,table (make-hash-table :test 'equal)))
       (lambda (&rest ,args)
         (or #1=(gethash ,args ,table)
             (setf #1# (apply (lambda ,lambda-list . ,body) ,args)))))))

(defmacro defmemo (name lambda-list &body body)
  `(setf (symbol-function ',name)
         (lambda-memo ,lambda-list . ,body)))

(defun new-tree-traverse (test transform)
  (labels
      ((recur (obj)
         (typecase obj
           (null nil)
           (atom (if (funcall test obj) (funcall transform obj) obj))
           (cons (cons (recur (car obj)) (recur (cdr obj)))))))
    #'recur))
(defun tree-traverse (tree test transform)
  (funcall (new-tree-traverse test transform) tree))
(defmacro tree-leaves (tree test transform)
  `(tree-traverse ,tree
                  (lambda (a) (declare (ignorable a)) ,test)
                  (lambda (a) (declare (ignorable a)) ,transform)))

(defmacro fn (&body lambda-form)
  (let ((count 0) (ctable (make-hash-table)) (atable (make-hash-table)))
    (tree-leaves lambda-form
      (and (symbolp a)
           (eq #\_ (char (symbol-name a) 0))
           (not (gethash a atable)))
      (prog1 a (setf (gethash count ctable) a)
        (setf (gethash a atable) t)
        (incf count)))
    (let ((args (loop for i from 0 to (- count 1) 
                      collect (gethash i ctable))))
      `(lambda ,args ,@lambda-form))))

(defmacro map-destructuring ((bindings &body body) &rest lists)
  (gensyms (sequences)
    `(map 'list
       (lambda (&rest ,sequences)
         (destructuring-bind ,bindings ,sequences
           . ,body))
       . ,lists)))

(defmacro capture (bindings &body body)
  `(let ,(map 'list (lambda (b) (list b b)) bindings)
     . ,body))
