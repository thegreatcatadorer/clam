;;;; A method system with arbitrary type specifiers.
;;;; As matches and type comparisons are memoized,
;;;; all type specifiers should be pure.
;;;; More general methods should be defined later.

(defpackage :generics
  (:use :cl :modules :symbols :macros :controls)
  (:export "PARSE-LAMBDA-LIST" "SPECIALIZE-LAMBDA-LIST" "DEFINE-GENERIC"
           "SEEK-GENERIC"))
(in-package :generics)

(defvar *method-table* (make-hash-table :test 'eq))

(defun lambda-keyword-p (symbol)
  (and (symbolp symbol)
       (member symbol '(&optional &rest &key &aux &allow-other-keys))))

(defmacro states (&body body)
  (let* ((states-list (map 'list #'car body))
         (body (map 'list #'cdr body)))
    `(block nil
       (tagbody . ,(mapcan #'cons states-list body)))))

(defun parse-lambda-list (list)
  (let ((list* list)
        defaults optionals rest keys)
    (macrolet ((shift (state)
                 `(progn (pop list*) (go ,state)))
               (error-misplaced (keyword)
                 `(error "specialize-lambda-list: Misplaced ~a in ~a!" ,keyword list)))
      (macros:after-injection states
        (map 'list
             (lambda (form)
               `(,(car form)
                 (let ((head (car list*)))
                   (declare (ignorable head))
                   . ,(cdr form))))
          '((:start
             (case head
               (&optional (shift :optional))
               ((&rest &key &allow-other-keys &aux nil) (go :optional))
               (t (if (and (consp head) (consp (car head)))
                      (push head defaults)
                      (push `(,head t) defaults))
                  (shift :start))))
           (:optional
             (case head
               (&optional (error-misplaced head))
               (&rest (shift :rest))
               (&key (shift :key))
               ((&allow-other-keys &aux nil) (go :key))
               (t (if (consp head)
                      (push head optionals)
                      (push `(,head t) optionals))
                  (shift :optional))))
           (:rest
             (setf rest (if (consp head) head `(,head t)))
             (pop list*) (shift :key))
           (:key
             (case head
               ((&optional &rest &key) (error-misplaced head))
               (&allow-other-keys (if (null (cdr list*)) (go :end)
                                      (error-misplaced head)))
               ((&aux nil) (go :end))
               (t (if (and (consp head) (consp (car head)))
                      (push head keys)
                      (push `(,head t) keys))
                  (shift :key))))
           (:end
             (return (values (nreverse defaults) (nreverse optionals)
                             rest (nreverse keys))))))))))

(defun specialize-lambda-list (name list function)
  (multiple-value-bind (defaults optionals rest keys)
      (parse-lambda-list list)
    (declare (ignore keys))
    (labels
        ((add-defaults (rest table)
           (if (null rest) (add-optionals optionals table)
               (let* ((type (cadar rest))
                      (check (lambda (args)
                               (and args (controls:is-type-p (car args) type))))
                      (next (new-vector))
                      (clause (list check #'cdr (add-defaults (cdr rest) next))))
                 (vector-push-extend clause table))))
         (add-optionals (rest table)
           (if (null rest) (add-rest table)
               (let* ((type (cadar rest))
                      (check (lambda (args)
                               (or (null args) (controls:is-type-p (car args) type))))
                      (next (new-vector))
                      (clause (list check #'cdr (add-optionals (cdr rest) next))))
                 (vector-push-extend clause table))))
         (add-rest (table)
           (if (null rest) (add-keys table)
               (let* ((type (cadr rest))
                      (check (lambda (args) (controls:is-type-p args type)))
                      (next (new-vector))
                      (clause (list check #'identity (add-keys next))))
                 (vector-push-extend clause table))))
         (add-keys (table) ; Does not typecheck keys
           (let* ((check (constantly t))
                  (clause (list check #'error function)))
             (vector-push-extend clause table))))
      (add-defaults defaults
                    (or #1=(gethash name *method-table*)
                        (setf #1# (new-vector)))))))

(defmacro define-generic (name args &body body)
  (let ((lambda-list (map 'list #'car/id args)))
    `(eval-when (:compile-toplevel)
       (specialize-lambda-list ',name ',args
         (lambda ',lambda-list . ,body)))))

(macros:defmemo seek-generic (name args)
  (loop named outer
        with table = (gethash name *method-table*)
        with args = args
        do (loop named inner
                 for clause across table
                 do (destructuring-bind (check step next) clause
                      (when (funcall check args)
                        (if (vectorp next)
                            (return-from inner
                                         (psetq args (funcall step args)
                                                table next))
                            (return-from outer next))))
                 finally (error "No clause matched in table ~a with args ~a!" table args))))
