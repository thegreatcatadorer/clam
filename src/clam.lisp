(defpackage :clam
  (:use :cl :modules :symbols :macros :lists :anaphors :controls :objects
        :classes :generics :trees))
