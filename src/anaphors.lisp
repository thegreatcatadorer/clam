(defpackage :anaphors
  (:use :cl :modules :symbols :macros :controls)
  (:export "NLAMBDA" "ALAMBDA" "ALAMBDA-MEMO" "DLAMBDA" "SELF" "ALOOP" "KLOOP"
           "RECURSIVE" "TAIL-RECURSIVE" "KEY-RECURSIVE"))
(in-package :anaphors)

(defmacro nlambda (name lambda-list &body body)
  `(labels
       ((,name ,lambda-list . ,body))
     #',name))

(defmacro alambda (lambda-list &body body)
  `(nlambda recur ,lambda-list . ,body))

(defmacro alambda-memo (lambda-list &body body)
  (gensyms (function)
    `(let (,function)
       (labels ((recur (&rest args)
                  (apply ,function args)))
         (setq ,function (lambda-memo ,lambda-list . ,body))
         #'recur))))

(defmacro dlambda (&body forms)
  (gensyms (form args)
    `(nlambda self (,form &rest ,args)
       (case ,form
         ,@(mapcar (lambda (f) `(,(car f) 
                                  (apply (lambda ,@(cdr f)) ,args)))
            forms)))))

(defmacro aloop (name lambda-list &body body)
  (gensyms (start)
    `(lambda ,lambda-list
       (macrolet
           ((,name ,lambda-list
              `(progn
                 (psetf ,@,@(map 'list (lambda (b) ``(,',b ,,b))
                              lambda-list))
                 (go ,',start))))
         (tagblock ,name ,start . ,body)))))

(defmacro kloop (name lambda-list &body body)
  (let ((predicates
          (map 'list
            (lambda (b)
              (gensym
                (concatenate 'string
                   (symbol-name b) "-P")))
            lambda-list)))
    (gensyms (start)
      `(lambda ,lambda-list
         (macrolet
             ((,name (&key . ,(map 'list (lambda (b p) `(,b nil ,p))
                                   lambda-list predicates))
                `(progn
                   (psetf ,@,@(map 'list (lambda (b p) `(when ,p `(,',b ,,b)))
                                lambda-list predicates))
                   (go ,',start))))
           (tagblock ,name ,start . ,body))))))

(with-bindings-do
    ((name macro)
     ('recursive 'nlambda) ('tail-recursive 'aloop) ('key-recursive 'kloop))
  `(defmacro ,name (name bindings &body body)
     (let ((lambda-list (map 'list #'car/id bindings))
           (arguments (map 'list (lambda (b) (car? (cdr? b))) bindings)))
       `(funcall (,',macro ,name ,lambda-list . ,body) . ,arguments))))
