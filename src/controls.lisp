(defpackage :controls
  (:use :cl :modules :symbols :macros)
  (:export "IS-TYPE-P" "WHERE" "WHERE*" "WHILE" "UNTIL" "AWHILE" "AUNTIL"
           "DEF/ID" "DEF?" "CAR/ID" "CDR/ID" "LIST/ID" "CONS/ID" "CAR?" "CDR?"
           "AIF" "OREQL" "PUSH-ALL" "GATHERING" "NEST" "ACCF" "NEW-VECTOR"
           "ALTERNATE" "INTERSECT" "WITH-ENUM" "TAGBLOCK" "TAGPROGN"
           "REDUCE-STACK" "REDUCE-STACK*" "FCONS" "FAPPEND" "COMPOSE" "PIPE"
           "GATHER" "COMPOSE*" "PIPE*" "STACK-CONS" "DEFER" "WITH"))
(in-package :controls)

(defmemo is-type-p (object type)
  (typep object type))

(defmacro with (symbol binding &body body)
  `(let ((,symbol ,binding)) . ,body))

(with-bindings-do ((wv lv) ('where 'let) ('where* 'let*))
  `(defmacro ,wv (body &body bindings)
     `(,',lv ,(loop for b in bindings collect `(,(car b) ,(cadr b)))
        ,@body)))

(with-bindings-do ((name expr) ('while 'unless) ('until 'when))
  `(defmacro ,name ((test &optional result) &body body)
     (gensyms (start)
       `(block nil
          (tagbody ,start (,',expr ,test (return ,result))
            ,@body)))))

(with-bindings-do ((name expr) ('awhile 'unless) ('auntil 'when))
  `(defmacro ,name ((test &optional result) &body body)
     (gensyms (start)
       `(block nil
          (let (test)
            (tagbody ,start
              (setf test ,test) (,',expr test (return ,result))
              ,@body))))))

(with-bindings-do ((def sym ret) ('def/id '/id 'value) ('def? '? 'nil))
  `(defmacro ,def (op pred &key op-args pred-args fn-args)
     `(defun ,(symb op ',sym) (value ,@fn-args)
        (if (,pred value ,@pred-args)
            (,op value ,@op-args)
            ,',ret))))

(def/id car consp)
(def/id cdr consp)
(def/id list atom)
(def/id cons atom
  :fn-args (&optional default)
  :op-args (default))

(def? car consp)
(def? cdr consp)

(defmacro aif (test then &rest else)
  `(let ((test ,test))
     (if test
         ,then
         (progn ,@else))))

(defmacro oreql (keyform &rest values)
  `(or ,@(loop for v in values collecting (list 'eql keyform v))))

(defmacro push-all (list &rest lists)
  `(setf ,list (nconc ,@lists)))

(defmacro gathering (&body body)
  (let ((name (gensym)))
    `(let ((,name nil))
       (labels
           ((gather (&rest list)
              (when list
                (push (car list) ,name)
                (apply #'gather (cdr list)))))
         ,@body)
       (nreverse ,name))))

(defmacro nest (&body body)
  (loop
    with rbody = (reverse body)
    with exprs = (car rbody)
    for expr in (cdr rbody)
    do (setf exprs (nconc expr (list exprs)))
    finally (return exprs)))

(defun accf (function count &optional (accumulator #'list))
  (apply accumulator
         (gathering
           (dotimes (i count)
             (gather (funcall function))))))

(defun new-vector (&optional (size 0))
  (make-array (list size) :adjustable t :fill-pointer size))

(defmacro with-enum (bindings &body body)
  `(symbol-macrolet ,(loop for i from 1 for b in bindings
                           collect `(,b ,i))
     . ,body))

(defmacro tagblock (name &body body)
  "TAGBODY with an implicit BLOCK and RETURN-FROM."
  `(block ,name
     (tagbody ,@(butlast body)
       (return-from ,name ,@(last body)))))

(defmacro tagprogn (&body body)
  "TAGBODY that returns the last expression (assumed not to be a tag)."
  `(tagblock ,(gensym) . ,body))

(defun alternate (&rest functions)
  (flet ((alt2 (f1 f2)
           (lambda (&rest args)
             (or (apply f1 args) (apply f2 args)))))
    (if functions
        (reduce #'alt2 functions :from-end t)
        (constantly nil))))

(defun intersect (&rest functions)
  (flet ((int2 (f1 f2)
           (lambda (&rest args)
             (and (apply f1 args) (apply f2 args)))))
    (if functions
        (reduce #'int2 functions :from-end t)
        (constantly t))))

(defun reduce-stack (stack obj)
  (loop for f in stack
        do (setf obj (funcall f obj)))
  obj)

(defun reduce-stack* (stack &rest args)
  (loop for f in stack
        do (setf args (apply f args)))
  args)

(defmacro stack-cons (stack captures lambda-list &body body)
  `(cons
     ((lambda ,captures
        (lambda ,lambda-list . ,body))
      . ,captures)
     ,stack))

(defmacro defer (stack captures lambda-list &body body)
  `(setf ,stack (stack-cons ,stack ,captures ,lambda-list . ,body)))

(defun fcons (function1 function2)
  (lambda (&rest args)
    (funcall function1 (apply function2 args))))
(defun fappend (function1 function2)
  (lambda (&rest args)
    (funcall function2 (apply function1 args))))

(defun compose (&rest functions)
  (cond ((null functions) #'identity)
    ((null (cdr functions)) (car functions))
    (t (lambda (&rest args)
         (let ((result (apply (car functions) args)))
           (dolist (f (cdr functions))
             (setf result (funcall f result)))
           result)))))
(defun compose* (&rest functions)
  (if (null functions) #'identity
      (lambda (&rest args)
        (let ((result args))
          (dolist (f functions)
            (setf result (apply f result)))
          result))))
(defun pipe (&rest functions)
  (apply #'compose (reverse functions)))
(defun pipe* (&rest functions)
  (apply #'compose* (reverse functions)))

#|(defvar *stack*)
(defun push-frame ()
  (vector-push-extend 'frame *stack*))
(defun push-function (function)
  (vector-push-extend function *stack*))
(defun reduce-stack (obj)
  (loop
    (let ((head (vector-pop *stack*)))
      (if (eq head 'frame) (return obj)
          (setf obj (funcall head obj))))))
(defun reduce-stack* (&rest objs)
  (loop
    (let ((head (vector-pop *stack*)))
      (if (eq head 'frame) (return objs)
          (setf objs (apply head objs))))))
(defmacro defer (captures lambda-list &body body)
  `(push-function
     ((lambda ,captures
        (lambda ,lambda-list . ,body))
      . ,captures)))|#
