(defpackage :objects
  (:use :cl :modules :symbols :macros :anaphors :controls :trees)
  (:export "DERIVE" "PGET" "PREM" "PFOR" "PCALL" "PAPPEND"
           "COPY-PROTO" "*SELF*" "WITH-SELF-BINDINGS"))
(in-package :objects)

;;; Prototype system below.
;;; All methods should use WITH-SELF-BINDINGS to inspect and modify the visible
;;; bound value, rather than the closest one.

(declaim (inline pget pset prem pfor pappend copy-proto))
(defun pget (key proto)
  (select-tree key proto))
(defun pset (key value proto)
  (add-tree key value proto))
(defun (setf pget) (key proto update)
  (setf (select-tree key proto) update))
(defun prem (key proto)
  (remove-tree key proto))
(defun pfor (function proto)
  (for-tree function proto))
(defun pappend (&rest derivations)
  (apply #'append-tree derivations))
(defun copy-proto (proto)
  (dup-tree proto))

(defun pcall (proto method &rest args)
  "Applies a method from the prototype to the given arguments.
Dynamically binds *SELF* to the prototype."
  (let ((*self* proto))
    (declare (special *self*))
    (apply (pget method proto) args)))

(defmacro with-self-bindings (bindings &body body)
"Replaces references to the bindings with references to the most recent PCALL."
  (let ((bindings (map 'list (lambda (b) (if (consp b) b (list b b)))
                             bindings)))
    `(symbol-macrolet ,(map-destructuring (((s b)) `(,s (pget ',b *self*)))
                                          bindings)
       (declare (special *self*))
       . ,body)))

(defmacro derive (derivations &body forms)
  (gensyms (proto form)
    `(let (,proto)
       (setf ,proto (pappend ,@derivations))
       (dolist (,form (list ,@(map 'list (lambda (f) `(list ',(car f) ,(cadr f))) forms)))
         (setf (pget (car ,form) ,proto) (cadr ,form)))
       ,proto)))
