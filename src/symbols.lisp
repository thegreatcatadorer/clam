(defpackage :symbols
  (:use :cl :modules)
  (:export "TO-STRING" "TO-READABLE" "MKSTR" "SYMB" "TO-KEYWORD" "ALIAS"))
(in-package :symbols)

(defun to-string (obj)
  (with-output-to-string (s)
    (princ obj s)))
(defun to-readable (obj)
  (with-output-to-string (s)
    (print obj s)))

(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))
(defun symb (&rest args)
  (values (intern (apply #'mkstr args))))
(defun to-keyword (&rest args)
  (values (intern (apply #'mkstr args) :keyword)))

(defmacro alias (name function)
  `(defmacro ,name (&rest args)
     (cons (quote ,function) args)))
