(defsystem "clam"
  :components ((:file "modules")
               (:file "symbols" :depends-on ("modules"))
               (:file "macros" :depends-on ("symbols"))
               (:file "controls" :depends-on ("macros"))
               (:file "generics" :depends-on ("controls"))
               (:file "anaphors" :depends-on ("controls"))
               (:file "lists" :depends-on ("anaphors"))
               (:file "trees" :depends-on ("anaphors"))
               (:file "classes" :depends-on ("anaphors"))
               (:file "objects" :depends-on ("anaphors"))
               (:file "clam" :depends-on
                      ("modules" "symbols" "macros" "lists" "controls"
                       "generics" "anaphors" "classes" "trees" "objects"))))
