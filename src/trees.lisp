(defpackage :trees
  (:use :cl :modules :symbols :macros :anaphors :controls)
  (:export "SELECT-TREE" "ADD-TREE" "SUB-TREE" "REMOVE-TREE" "MAP-TREE"
           "MAP-TREES" "MAP-TREES-PARALLEL" "FOR-TREE" "FOR-TREES"
           "FOR-TREES-PARALLEL" "TREE-ALIST" "ALIST-TREE" "APPEND-TREE"
           "MAKE-HASH-TREE" "DUP-TREE"))
(in-package :trees)

;; Hexadecimal logarithm of the size of SXHASH.
(defvar sxhash-log 16)

(declaim (inline lsh lshx modb modx))
(defun lsh (integer &optional (count 1))
  (ash integer (- count)))
(defun lshx (integer)
  (lsh integer 4))
(defun modb (digits integer)
  (logand (1- (expt 2 digits)) integer))
(defun modx (integer)
  (modb 4 integer))

(defun copy-vector (vector)
  (map 'vector #'identity vector))

(defstruct node
  (branches #.(make-array '(16) :initial-element nil)
            :type (simple-vector 16)))
(defun copy-node* (node)
  (make-node :branches (copy-vector (node-branches node))))
(defmethod ref ((place integer) (collection node))
  (declare (type (integer 0 15) place))
  (aref (node-branches collection) place))
(defmethod (setf ref) (update (place integer) (collection node))
  (declare (type (integer 0 15) place))
  (setf (aref (node-branches collection) place) update))
(defun node-add-pair (node place value)
  (declare (type (integer 0 15) place))
  (let ((node (copy-node* node)))
    (setf (aref (node-branches node) place) value)
    node))
(defun node-from-pair (place value)
  (node-add-pair (make-node) place value))

(defstruct (hash-tree (:include node)))

(defun select-tree (key tree)
  (tail-recursive recur ((i sxhash-log) (s (sxhash key)) (br tree))
    (cond ((null br) nil)
      ((zerop i) (values (cdr br) (car br)))
      (t (recur (1- i) (lshx s) (ref (modx s) br))))))
(defmethod ref (place (collection hash-tree))
  (select-tree place collection))

(defun hash-node (depth hash key value &optional (stack nil))
  (tail-recursive recur ((i depth) (s hash) (stack stack))
    (cond ((zerop i) (reduce-stack stack (cons key value)))
      (t (recur (1- i) (lshx s)
           (stack-cons stack (s) (v) (node-from-pair (modx s) v)))))))

(defun add-tree (key value tree)
  (tail-recursive recur
      ((i sxhash-log) (s (sxhash key)) (br tree) (stack nil))
    (cond ((zerop i) (reduce-stack stack (cons key value)))
      ((null br) (hash-node i s key value stack))
      (t (recur (1- i) (lshx s) (ref (modx s) br)
           (stack-cons stack (br s) (v)
             (node-add-pair br (modx s) v)))))))

(defun sub-tree (key tree)
  (tail-recursive recur
      ((i sxhash-log) (s (sxhash key)) (br tree) (stack nil))
    (cond ((or (zerop i) (null br)) (reduce-stack stack nil))
      (t (recur (1- i) (lshx s) (ref (modx s) br)
           (stack-cons stack (br s) (v)
             (node-add-pair br (modx s) v)))))))

(defun (setf select-tree) (update key tree)
  (tail-recursive recur
      ((i sxhash-log) (s (sxhash key)) (br tree) (stack nil))
    (cond ((zerop i) (reduce-stack stack (cons key update)))
      ((null br) (hash-node i s key update stack))
      (t (recur (1- i) (lshx s) (ref (modx s) br)
           (stack-cons stack (br s) (v)
             (setf (ref (modx s) br) v) br)))))
  update)
(defmethod (setf ref) (update place (collection hash-tree))
  (setf (select-tree place collection) update))

(defun remove-tree (key tree)
  (tail-recursive recur
      ((i sxhash-log) (s (sxhash key)) (br tree) (stack nil))
    (cond ((or (zerop i) (null br)) (reduce-stack stack nil))
      (t (recur (1- i) (lshx s) (ref (modx s) br)
           (stack-cons stack (br s) (v)
             (setf (ref (modx s) br) v) br))))))

(defun lazy-map-tree (function tree)
  (recursive delay ((i sxhash-log) (br tree))
    (when br
      (if (= i 0) (cons (car br) (funcall function (car br) (cdr br)))
          (map 'vector (lambda (v) (lambda () (delay (1- i) v)))
               (node-branches br))))))

(defun map-tree (function tree)
  (let ((tree* (lazy-map-tree function tree)))
    (tail-recursive recur ((force-list (list (cons sxhash-log tree*))))
      (if force-list
          (let ((i (caar force-list)) (br (cdar force-list)))
            (if (and br (not (= i 0)))
                (recur
                  (append
                    (loop for i from 0 to 15
                          collect (cons (1- i) (setf (ref i br) (funcall (ref i br)))))
                    (cdr force-list)))
                (recur (cdr force-list))))
          tree*))))

(defun dup-tree (tree)
  (map-tree (lambda (a b) (declare (ignore a)) b) tree))

(with-bindings-do
    ((lazy strict consp)
     ('lazy-map-trees 'map-trees 'some)
     ('lazy-map-trees-parallel 'map-trees-parallel 'every))
  `(progn
     (defun ,lazy (function &rest trees)
       (recursive delay ((i sxhash-log) (brs trees))
         (when (,consp #'identity brs)
           (if (= i 0)
               (let ((sym (some #'identity (map 'list #'car brs))))
                 (cons sym (apply function sym (map 'list #'cdr brs))))
               (map 'vector
                    (lambda (j)
                      (lambda ()
                        (delay (1- i)
                              (map 'list (lambda (br) (when br (ref j br)))
                                    brs))))
                    #(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15))))))
     (defun ,strict (function &rest trees)
       (let ((trees* (apply #',lazy function trees)))
         (tail-recursive recur ((force-list (list (cons sxhash-log trees*))))
           (if force-list
               (let ((i (caar force-list)) (br (cdar force-list)))
                 (if (and br (not (= i 0)))
                     (recur
                       (append
                         (loop for i from 0 to 15
                               collect (cons (1- i) (setf (ref i br) (funcall (ref i br)))))
                         (cdr force-list)))
                     (recur (cdr force-list))))
               trees*))))))

(defun for-tree (function tree)
  (tail-recursive recur ((queue (list (cons sxhash-log tree))))
    (when queue
      (let ((i (caar queue)) (br (cdar queue)))
        (cond ((and (= i 0) br) (funcall function (car br) (cdr br)))
          (br (recur
                (append
                  (loop for j from 0 to 15
                        collect (cons (1- i) (ref j br)))
                  (cdr queue))))
          (t (recur (cdr queue))))))))

(with-bindings-do
    ((name nullp) ('for-trees 'every) ('for-trees-parallel 'some))
  `(defun ,name (function &rest trees)
     (tail-recursive recur ((queue (list (cons sxhash-log trees))))
       (when queue
         (let ((i (caar queue)) (brs (cdar queue)))
           (cond ((,nullp #'null brs) (recur (cdr queue)))
             ((= i 0)
              (apply function (some #'car brs) (map 'list #'cdr brs)))
             (t (recur
                  (append
                    (loop for j from 0 to 15
                          collect (cons (1- i)
                                        (map 'list
                                             (lambda (br) (when br (ref j br)))
                                             brs)))
                    (cdr queue))))))))))

(defun alist-tree (alist)
  (let (tree)
    (dolist (a alist) (setf (select-tree (car a) tree) (cdr a)))
    tree))

(defun tree-alist (tree)
  (gathering (for-tree (compose #'gather #'cons) tree)))

(defun append-tree (&rest trees)
  (apply #'map-trees
    (lambda (&rest brs) (some #'identity brs))
    trees))
